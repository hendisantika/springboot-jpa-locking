package com.hendisantika.springbootjpalocking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJpaLockingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJpaLockingApplication.class, args);
    }

}

