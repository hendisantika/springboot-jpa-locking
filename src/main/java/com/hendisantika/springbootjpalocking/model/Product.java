package com.hendisantika.springbootjpalocking.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jpa-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 07:52
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Product {
    @Id
    private UUID id;

    private String name;

    private int stock;

    protected Product() {
        this.id = UUID.randomUUID();
    }

    public Product(String name, int stock) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.stock = stock;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
