package com.hendisantika.springbootjpalocking;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jpa-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 07:56
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TestHelper {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void reset() {
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
        entityManager.getMetamodel().getEntities().forEach(type -> {
            String entityName = type.getName();
            entityManager.createQuery(String.format("DELETE FROM %s", entityName)).executeUpdate();
        });
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
    }

    @Transactional
    public void resetPostgres() {
        entityManager.getMetamodel().getEntities().forEach(type -> {
            String entityName = type.getName();
            entityManager.createQuery(String.format("DELETE FROM %s", entityName)).executeUpdate();
        });
    }
}
